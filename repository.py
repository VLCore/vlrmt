#!/usr/bin/env python
"""Abstraction over the PACKAGES.TXT file. This module defines a Package object
providing the data estructure for holding package information. A factory
function will parse PACKAGES.TXT and returns a sequence of Package objects.

"""


__author__ = "rbistolfi"
__credits__ = ""


import re
from distutils.version import LooseVersion as Version


class Package(object):
    """Static class that represents a VL package."""

    def __cmp__(self, other):
        """uses version numbers to compare package objects."""

        if isinstance(other, Version):
            return cmp(Version(self.version), other)
        elif isinstance(other, Package):
            return cmp(Version(self.version), Version(other.version))
        else:
            raise TypeError("Expected Version or Package instance, got %r" %
                    other)

    def __init__(self, attr_list):
        """Initializes a Package object from a list of attributes."""

        self.filename, self.location, self.compressed, self.uncompressed, \
                self.required, self.conflicts, self.suggests, \
                self.description = [ i.strip() for i in attr_list ]

        self.name, self.version, self.arch, self.build = \
				self.filename.rsplit("-", 3)

        self.tag = self.build.rsplit(".", 1)[0]


def repository(packages_txt):
    """Parse PACKAGES.TXT file and return a list of Package objects.
    Argument can be an URL or a file path.

    """

    if 'http://' in packages_txt:
        from urllib2 import urlopen
        url_handler = urlopen(packages_txt)
        data = url_handler.read()
        url_handler.close()
    else:
        file_handler = open(packages_txt)
        data = file_handler.read()
        file_handler.close()

    regex = re.compile('''PACKAGE\sNAME:\s*(.*)
            PACKAGE\sLOCATION:\s*(.*)
            PACKAGE\sSIZE.*:\s*(.*)
            PACKAGE\sSIZE.*:\s*(.*)
            PACKAGE\sREQUIRED:\s*(.*)
            PACKAGE\sCONFLICTS:\s*(.*)
            PACKAGE\sSUGGESTS:\s*(.*)
            PACKAGE\sDESCRIPTION:\s*(.*)\Z''', re.DOTALL|re.VERBOSE)

    data = data.strip()
    data = data.split("\n\n")
    return ( Package(regex.search(i).groups()) for i in data )
