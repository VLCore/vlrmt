#-*- coding: utf-8 -*-


"""Find packages in vectorcontrib and report if they have been moved to the
repository.

"""


import os
import re
import shutil
import repository
import utilities
import settings


class Package(repository.Package):

    def __init__(self, filename):
        self.filename = filename
        self._split = filename.rsplit("-",3)
        self.packager = None
        self.dest_location = None
        self.location = None

    def __eq__(self, other):
        assert hasattr(other, "filename"), "%r has not a filename" % other
        return self.filename == other.filename

    @classmethod
    def from_filename(cls,filename):
        """Create a Package instance from a filename."""
        return Package(filename)

    @property
    def name(self):
        return self._split[0]

    @property
    def version(self):
        return self._split[1]

    @property
    def arch(self):
        return self._split[2]

    @property
    def build(self):
        return self._split[3].split("vl")[0]

    @property
    def vector_version(self):
        """Return the vector version this package belongs to.
        An ad-hoc solution has to be given for now, as a trustable algorithm is
        not possible with our current naming scheme.

        """
        build_number, version = self._split[3].split("vl")
        version = version.rsplit(".", 1)[0]
        if "x86_64" in self.arch:
            prefix = "VL64-"
        else:
            prefix = "veclinux-"
        version = ".".join([version[:-1], version[-1]])
        return prefix + version

    @property
    def extension(self):
        return self._split[3].rsplit(".", 1)[1]

    @property
    def source(self):
        """Determine the location of the source folder"""

        return self.location.rsplit("/", 2)[0]


def is_package(filename):
    """Return True if filename is a package."""

    _, ext = filename.rsplit(os.path.extsep, 1)
    if ext:
        return any(ext in i for i in ("tlz", "tgz", "txz"))


class VectorContribModel(object):

    def __init__(self):

        self.hints = None

        # paths
        self.repo_root = settings.repo_root
        self.vectorcontrib = settings.vectorcontrib

        # available vector versions
        self.versions = settings.versions

        # possible combinations of source/target locations
        combinations = [("vectorcontrib", "testing"), ("testing", "patches"),
                ("testing", "extra")]

        # states
        self.current_version = utilities.Cycle(self.versions.keys(),
                "veclinux-7.0")
        self.current_combination = utilities.Cycle(combinations, combinations[0])

        # cache by package name
        self.cache = {}

    def get_target(self):

        repo_root = self.repo_root
        version = str(self.current_version)
        repository = self.current_combination.state[1]

        return os.path.join(repo_root, version, repository)

    def get_source(self):

        source_name = self.current_combination.state[0]
        if "contrib" in source_name:
            path = self.vectorcontrib
        else:
            root = self.repo_root
            version = str(self.current_version)
            repo = source_name
            path = os.path.join(root, version, repo)
        return path

    def get_source_path(self):
        root = self.repo_root
        version = str(self.current_version.state)
        source_dir = "source"
        return os.path.join(root, version, source_dir)

    def get_packages(self):
        log = open("log", "w")
        source = self.get_source()
        version = str(self.current_version.state)
        version_re = self.versions[version][0]
        if "contrib" in source:
            path = self.vectorcontrib
        else:
            version = str(self.current_version)
            repo, _ = self.current_combination.state
            path = os.path.join(self.repo_root, version, repo)

        packages = find_packages(path, version_re=version_re,
                hint_dest_dir=self.hints)

        self.cache = {}
        for i in packages:
            self.cache[i.filename] = i
        log.write(source + "\n")
        log.write(path + "\n")
        log.write(str(self.current_version.state))
        log.write(str(self.current_combination.state))
        log.write(str(packages))
        log.close()
        return self.cache.values()

    def byfilename(self, filename):
        return self.cache[filename]

    def byname(self, name):
        for k in self.cache.iterkeys():
            kname = k.rsplit("-", 3)[-1]
            if name == kname:
                return self.cache[k]

    def next_combination(self):

        self.current_combination.next()

    def next_version(self):

        self.current_version.next()


def find_packages(vectorcontrib_root, version_re=".*", hint_dest_dir=None):
    """Yields each package in the vectorcontrib area.

    """

    CATEGORIES = ("a", "ap", "d", "k", "kde", "kdei", "l", "n", "tcl", "x",
        "xap", "xfce")

    version_matchs = re.compile(version_re).match

    for root, dir, files in os.walk(vectorcontrib_root):
        # Ignore top directory
        if root == vectorcontrib_root:
            continue
        for file in files:
            if is_package(file) and version_matchs(file):
                package = Package.from_filename(file)
                package.location = os.path.join(root, file)
                package.packager = package.location.split("/")[1]
                dest = utilities.suggest_location(package, hint_dest_dir)
                if dest:
                    package.dest_location = utilities.Cycle(CATEGORIES,
                            initial=dest)
                else:
                    package.dest_location = utilities.Cycle(CATEGORIES,
                            initial="xap")

                yield package
