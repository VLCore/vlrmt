#-*- coding: utf-8 -*-


"""Utilities for validating contributed packages."""


class ValidationError(Exception):
    """Raised when a package is invalid"""
    pass


class Validator(object):
    """Base class for validators"""

    def __init__(self, package):
        self.package = package

    def validate(self, package):
        raise NotImplementedError


class FileNameValidator(Validator):
    """Validates the package filename"""
    def validate(self):
        return True


class SourceValidator(Validator):
    """Validates the source and directory tree."""

    def validate(self):
        return True


class DependencyValidator(Validator):
    """Checks that there is no offending deps in the package."""

    def validate(self):
        return True

