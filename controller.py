#-*- coding: utf-8 -*-


"""Controller module for the VectorLinux repository maintenance tool"""


import vectorcontrib
import utilities
import repository
import view
import shutil
import os


class Controller(object):
    """Controller class for the VL repo tool.

    """
    def __init__(self):

        #get old data for target hinting
        url = "http://vectorlinux.osuosl.org/veclinux-6.0/testing/PACKAGES.TXT"
        hint_repo = repository.repository(url)

        #model
        self.model = vectorcontrib.VectorContribModel()
        self.model.hints = hint_repo
        self.packages = list(self.model.get_packages())

        #window setup
        self.labels = labels = [" id ", " filename", "packager", "target"]
        widths = 4, 50, 15, 20
        self.window = window = view.PackageList(self.packages, labels, widths)

        version = self.model.current_version.state
        source, target = self.model.current_combination.state
        self.header_template = "\n VectorLinux Repository Maintenance Tool | " \
                "%s - %s / %s \n"
        window.header = self.header_template % (version, source, target)
        window.footer = "\n j/k: up/down | f: toggle selection | M: move | "\
                "D: delete | c: cycle target | F1: cycle repos | F2: cycle " \
                "versions | q: quit\n"

        ##register callbacks
        #callbacks on package selection
        window.add_action(self.move_package, "M")
        window.add_action(self.del_package, "D")
        #callbacks on focused package
        window.add_action(self.move_package, "m", on_focused=True)
        window.add_action(self.del_package, "d", on_focused=True)
        window.add_action(self.cycle_dest, "c", on_focused=True)
        #callbacks on environment
        window.add_action(self.cycle_paths, "f1", no_args=True)
        window.add_action(self.cycle_version, "f2", no_args=True)

    def run(self):
        #run ui
        self.window.run()

    def move_package(self, package):
        """Move a package to the repository

        """
        #temp names
        current_location = package.location
        dest_location = str(package.dest_location)
        dest_dir = self.model.get_target()

        #check for sources
        root_dir = os.path.isdir(package.source)
        version_dir = os.path.isdir(os.path.join(package.source,
            package.version))
        sources_dest_dir = self.model.get_source_path()

        #check if target dir exists
        path = os.path.join(dest_dir, package.dest_location.state)
        if not os.path.exists(path):
            os.makedirs(path)

        #move the package
        shutil.move(package.location, path)

        #move sources
        if root_dir and version_dir and sources_dest_dir:
            shutil.move(package.source, sources_dest_dir)

        #update model and notify
        self.packages.remove(package)
        self.notify(package, "pkg-removed")

        #find deps and move them also
        slack_required = os.path.join(package.source, package.version,
                "slack-required")
        if os.path.exists(slack_required):
            with open(slack_required) as sr:
                for line in sr:
                    name = line.split(" ")[0]
                    if name in self.packages:
                        dep = self.model.byname(name)
                        self.move_package(dep)

    def del_package(self, package):
        """Delete a package from the contrib area

        """
        os.remove(package.location)
        self.packages.remove(package)
        self.notify(package, "pkg-removed")

    def cycle_paths(self):
        """Set source/dest to the next combination.
        Use this for switching source/target from contrib/testing to
        testing/extra or testing/patches.

        """
        self.model.next_combination()
        self.packages = self.window.items = list(self.model.get_packages())
        version = self.model.current_version.state
        source, target = self.model.current_combination.state
        self.window.header = self.header_template % (version, source, target)
        self.notify(self.packages, event="paths-changed")

    def cycle_version(self):
        """Set the current vector version to the next.

        """
        self.model.next_version()
        self.packages = self.window.items = list(self.model.get_packages())
        version = self.model.current_version.state
        source, target = self.model.current_combination.state
        self.window.header = self.header_template % (version, source, target)
        self.notify(self.packages, event="version-changed")

    def cycle_dest(self, package):
        """Set the category attribute (it happens to be also the destination
        folder inside the repository) in the package to the next available.

        """
        package.dest_location.next()
        self.notify(package, "pkg-modified")

    def notify(self, item, event="pkg-modified"):
        """Notify the UI about changes in the model.
        The event keyword arg can be modified, added, or removed

        """
        assert event in ("pkg-modified", "pkg-added", "pkg-removed",
                "paths-changed", "version-changed")
        self.window.update(item, event)


c = Controller()
c.run()
