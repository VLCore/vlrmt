#-*- coding: utf-8 -*-

"""Settings for the Vectorlinux Repository Maintenance Tool."""

# map a version directory in the repo root folder with a regex matching the
# proper package filename
versions = {
        "veclinux-7.0": (r".+i[3456]86-\d+vl70", r".+i[3456]86-\d+slack13\.\d+"),
        "VL64-7.0":     (r".+x86_64-\d+vl70", r".+\x86_64d+slack13\.\d+"),
        "veclinux-6.0": (r".+i[3456]86-\d+vl60", r".+i[3456]86-\d+slack12\.1"),
        "veclinux-5.9": (r".+i[3456]86-\d+vl59", r".+i[3456]86-\d+slack12\.0"),
        }

# the path to the vectorcontrib folder
vectorcontrib = "contrib_mock"

# the path to the repo root folder
repo_root = "repo_mock"

# repositories
# repositories = ("packages", "extra", "testing", "patches", "gsb")
