#-*- coding: utf-8 -*-

"""Utilities for the VL repo tool."""


from itertools import cycle
import os
import shutil


CATEGORIES = ("a", "ap", "d", "k", "kde", "kdei", "l", "n", "tcl", "x",
        "xap", "xfce")


class Cycle(object):
    """Cycle ad infinitum through the items, starting from initial."""

    def __init__(self, items, initial=None):

        self.items = items
        self.state = initial or items[0]

    def __str__(self):
        return str(self.state)

    def __unicode__(self):
        return unicode(self.__str__())

    def __iter__(self):
        return self

    def next(self):
        index = self.items.index(self.state)
        if index == len(self.items) - 1:
            state = self.items[0]
        else:
            state = self.items[index + 1]
        self.state = state
        return self.state


def translate_location(location):
    """Translate a location name from older repos to new repos."""

    #cleanup
    location = location.lstrip("./")

    OLD_CATEGORIES = ("base", "base-apps", "dev", "kernel", "kde", "kdei",
            "lib", "net", "tcl", "x", "x-apps", "xfce")
    table = dict(zip(OLD_CATEGORIES, CATEGORIES))
    return table.get(location, location)


def suggest_location(package, repository=None):
    """Try to guess the target location for a package by looking it up in
    another repository.

    """
    repository = repository or []
    for i in repository:
        if package.name == i.name:
            return translate_location(i.location)

