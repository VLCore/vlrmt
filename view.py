#-*- coding: utf-8 -*-


import urwid


class PackageList(object):

    urwid.command_map["j"] = "cursor down"
    urwid.command_map["k"] = "cursor up"
    urwid.command_map["h"] = "cursor left"
    urwid.command_map["l"] = "cursor right"
    urwid.command_map["esc"] = "cursor up"
    urwid.command_map["f"] = "activate"

    palette = [
        ('body',            'black',        'light gray',   'standout'),
        ('header',          'light green',  'black',        'bold'    ),
        ('footer',          'light gray',   'black'                   ),
        ('row active',      'light green',  'dark gray',    'bold'    ),
        ('row',             'dark green',   'black'                   ),
        ('button normal',   'dark gray',    'dark gray'               ),
        ('button select',   'dark gray',    'black'                   ),
        ('cols_header',     'white',        'dark gray'               ),
        ]

    def __init__(self, items, labels, widths, header=None, footer=None):

        assert len(labels) == len(widths), "Columns names and items have a"\
                " different len"

        # columns setup
        self.items = items
        self.labels = labels
        self.widths = widths

        # rows
        self.rows = rows = list(self._get_rows())

        # ui elements
        self.body = Body(rows)
        self.header = header
        self.footer = footer
        self.frame = None

        # functions to be applied to the selection
        self.actions = {}
        # functions to be applied with no args
        self.actions_on_env = {}
        # functions to be applied to the focused packages
        self.actions_on_focused = {}

    ## update ui

    def update(self, item, event="modified"):

        if event == "pkg-modified":
            #replace the row with a new one with updated values
            index = self.items.index(item)
            row = Row(item, index, self.widths)
            self.body.set_item(index, row)

        elif event in ("version-changed", "paths-changed"):
            self.frame.header = self._get_header()
            self.body = Body(list(self._get_rows()))
            self.frame.body = self._get_body()

        else:
            #the other events append or remove to the pkg list
            #recreate the body, we cant append or remove by urwid design
            self.body = Body(list(self._get_rows()))
            self.frame.body = self._get_body()


    ## actions

    def add_action(self, func, hotkey, on_focused=False, no_args = False):

        # check for on_focused and no_args
        assert not (on_focused and no_args), "Keyword args 'on_focused' and "\
                "'no_args' are mutualy exclusive"

        # check if hotkey is already in use
        used_keys = self.actions.keys() + self.actions_on_focused.keys() + \
                self.actions_on_env.keys()
        assert hotkey not in used_keys, "Key binding already in use"

        # map a hotkey to a function
        if on_focused:
            self.actions_on_focused[hotkey] = func
        elif no_args:
            self.actions_on_env[hotkey] = func
        else:
            self.actions[hotkey] = func

    ## loop

    def run(self):
        self.frame = self._get_frame()
        loop = urwid.MainLoop(self.frame, self.palette,
                unhandled_input=self.input)
        loop.run()

    def input(self, key):
        if key == "q":
            self.quit(0)

        if key in self.actions:
            func = self.actions[key]
            items = self.body.get_state()
            for package in items:
                func(package)

        elif key in self.actions_on_focused:
            func = self.actions_on_focused[key]
            item = self.body.get_focus()
            func(item)

        elif key in self.actions_on_env.keys():
            func = self.actions_on_env[key]
            func()

        else:
            pass

    def quit(self, arg):
        raise urwid.ExitMainLoop(arg)

    ## Helper methods
    #  Widgets

    def _get_header(self):
        header = urwid.AttrWrap(
                urwid.Text(self.header), "header")
        return header

    def _get_footer(self):
        footer = urwid.AttrWrap(
                urwid.Text(self.footer), "footer")
        return footer

    def _get_rows(self):
        if not self.items:
            text = urwid.Text(" Empty list ")
            text.get_widget = lambda: text
            yield text

        for index, package in enumerate(self.items):
            row = Row(package, index, self.widths)
            yield row

    def _get_pile(self):
        return self.body.get_widget()

    def _get_body(self):

        labels = [ ("fixed", width, urwid.Text(label)) for width, label
                in zip(self.widths, self.labels) ]

        header = urwid.Columns(labels)
        header = urwid.AttrWrap(header, "cols_header")
        header.get_widget = lambda: header # :(

        lw = urwid.SimpleListWalker([
            header,
            urwid.Divider(),
            self._get_pile(),
            urwid.Divider(),
            urwid.Divider(),
            ])
        lb = urwid.ListBox(lw)
        return lb

    def _get_frame(self):

        frame = urwid.Frame(self._get_body(), header=self._get_header(), footer=self._get_footer())
        self.frame = frame
        return frame


class Body(object):
    """Shows a Package list using an urwid.Pile.

        -- rows: a list of Row instances

    """

    def __init__(self, rows, style=None):

        self.rows = rows
        self.widget_list = [ i.get_widget() for i in rows ]
        self.style = style
        self.pile = None
        self._setup()

    def get_widget(self):
        return self.pile

    def get_state(self):
        return [ i.package for i in self.rows if i.is_checked() ]

    def get_focus(self):
        col = self.pile.get_focus()
        row = self.rows[self.pile.widget_list.index(col)]
        pkg = row.package
        return pkg

    def append(self, row):
        self.pile.widget_list.append(row.get_widget())

    def remove(self, row):
        # remove from our wrapper
        self.rows.remove(row)
        self._setup()

    def set_item(self, index, row):
        self.rows[index] = row
        self.pile.widget_list[index] = row.get_widget()
        self.pile.set_focus(index)

    def _setup(self):

        pile=urwid.Pile(list(self.widget_list))
        if self.style is not None:
            lb = urwid.AttrWrap(pile, self.style)
        self.pile = pile


class Row(object):
    """Shows a Package instance using an urwid.Columns

        -- package: a Package instance
        -- index: the index of the package in the package list
        -- col_width: a tuple with ints representing the width of each column

    """

    def __init__(self, package, index, col_width):
        self.package = package
        self.index = index
        self.col_width = col_width

        self.checkbox = urwid.CheckBox(package.filename)
        self.column = None

        self._setup()

    def is_checked(self):
        return self.checkbox.state

    def get_widget(self):
        return self.column

    def _setup(self):
        col = ("fixed", self.col_width[0], urwid.Text(str(self.index),
            "center"))
        col1 = ("fixed", self.col_width[1] , self.checkbox)
        col2 = ("fixed", self.col_width[2], urwid.Text(self.package.packager))
        col3 = ("fixed", self.col_width[3],
                urwid.Text(str(self.package.dest_location)))
        self.column =  urwid.AttrWrap(urwid.Columns([col, col1, col2, col3]),
                "row", "row active")

